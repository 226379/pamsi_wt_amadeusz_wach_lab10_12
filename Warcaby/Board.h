#pragma once
// 0 - brak, 1 - biale, 2 - czarne, 3 - biala damka, 4 - czarna damka
class Board {
	int ** array;
	int size;

	int _x;
	int _y;
	int _dir;

	bool inBoard(int x, int y) {
		if (0 <= x && size > x && 0 <= y && size > y)
			return true;
		return false;
	}

public:
	Board(int _size) {
		size = _size;
		array = new int *[size];
		for (int i = 0; i < size; i++)
			array[i] = new int[size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				array[i][j] = 0;
			}
		}
	}
	void init() {
		//czarne
		for (int i = 0; i < size / 2 - 1; i++) {
			for (int j = 0; j < size; j++) {
				if ((j + i) % 2 == 1) array[i][j] = 2;
			}
		}

		//bia�e

		for (int i = size / 2 + 1; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if ((j + i) % 2 == 1) array[i][j] = 1;
			}
		}
	}

	void display() {
		std::cout << "  ";
		for (int j = 0; j < size; j++) std::cout << j << " ";
		std::cout << std::endl;
		for (int i = 0; i < size; i++) {
			std::cout << i << " ";
			for (int j = 0; j < size; j++) {
				if (array[i][j] == 1)
					std::cout << "o ";
				else if (array[i][j] == 2)
					std::cout << "x ";
				else if (array[i][j] == 3)
					std::cout << "O ";
				else if (array[i][j] == 4)
					std::cout << "X ";
				else
					std::cout << "  ";
			}
			std::cout << std::endl;
		}
	}

	int rate(int player) {
		int rate = 0;
		int enemy = ((player == 1) ? 2 : 1);
		//ocena pionkow
		//funkcja trzech obszarow
		Area a1(0, 0, size - 1, size - 1, 15);
		Area a2(1, 1, size - 2, size - 2, 10);
		Area a3(2, 2, size - 3, size - 3, 5);

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				int weight = 1;
				if (a3.belong(i, j))
					weight = a3.getWeight();
				else if (a2.belong(i, j))
					weight = a2.getWeight();
				else if (a1.belong(i, j))
					weight = a1.getWeight();
				if (array[i][j] == player)
					rate += weight;
				if (array[i][j] == enemy)
					rate -= weight;
				if (array[i][j] == player + 2)
					rate += 3 * weight;
				if (array[i][j] == enemy + 2)
					rate -= 3 * weight;
			}
		}

		return rate;
	}

	bool move(int direction, int x, int y, int player, int steps = 1) {
		int enemy = ((player == 1) ? 2 : 1);
		int tmp = array[x][y];
		//pion
		if (array[x][y] == 1 || array[x][y] == 2) {
			switch (direction) {
			case 1: {
				//ruch o jeden
				if (inBoard(x - 1, y - 1)) {
					if (array[x - 1][y - 1] == enemy || array[x - 1][y - 1] == enemy + 2) {
						//pole zajete
						//czy jest bicie
						if (inBoard(x - 2, y - 2)) {
							if (array[x - 2][y - 2] == 0) {
								//sprawdz czy sa dalsze bicia/

								//zmien piony
								array[x][y] = 0;
								array[x - 1][y - 1] = 0;
								array[x - 2][y - 2] = tmp;
							}
						}
					}
					else if (array[x - 1][y - 1] == 0) {
						//przesuniecie pionka
						array[x][y] = 0;
						array[x - 1][y - 1] = tmp;
					}
				}
				break;

			}

			case 2: {
				//ruch o jeden
				if (inBoard(x - 1, y + 1)) {
					if (array[x - 1][y + 1] == enemy || array[x - 1][y + 1] == enemy + 2) {
						//pole zajete
						//czy jest bicie
						if (inBoard(x - 2, y + 2)) {
							if (array[x - 2][y + 2] == 0) {
								//sprawdz czy sa dalsze bicia/

								//zmien piony
								array[x][y] = 0;
								array[x - 1][y + 1] = 0;
								array[x - 2][y + 2] = tmp;
							}
						}
					}
					else if (array[x - 1][y + 1] == 0) {
						//przesuniecie pionka
						array[x][y] = 0;
						array[x - 1][y + 1] = tmp;
					}
				}
				break;

			}

			case 3: {
				//ruch o jeden
				if (inBoard(x + 1, y + 1)) {
					if (array[x + 1][y + 1] == enemy || array[x + 1][y + 1] == enemy + 2) {
						//pole zajete
						//czy jest bicie
						if (inBoard(x + 2, y + 2)) {
							if (array[x + 2][y + 2] == 0) {
								//sprawdz czy sa dalsze bicia/

								//zmien piony
								array[x][y] = 0;
								array[x + 1][y + 1] = 0;
								array[x + 2][y + 2] = tmp;
							}
						}
					}
					else if (array[x + 1][y + 1] == 0) {
						//przesuniecie pionka
						array[x][y] = 0;
						array[x + 1][y + 1] = tmp;
					}
				}
				break;

			}

			case 4: {
				//ruch o jeden
				if (inBoard(x + 1, y - 1)) {
					if (array[x + 1][y - 1] == enemy || array[x + 1][y - 1] == enemy + 2) {
						//pole zajete
						//czy jest bicie
						if (inBoard(x + 2, y - 2)) {
							if (array[x + 2][y - 2] == 0) {
								//sprawdz czy sa dalsze bicia/

								//zmien piony
								array[x][y] = 0;
								array[x + 1][y - 1] = 0;
								array[x + 2][y - 2] = tmp;
							}
						}
					}
					else if (array[x + 1][y - 1] == 0) {
						//przesuniecie pionka
						array[x][y] = 0;
						array[x + 1][y - 1] = tmp;
					}
				}
				break;

			}
			}
		}
		else if (array[x][y] == 0) {
			std::cout << "Puste!" << std::endl;
			return false;
		}
		else {
			switch (direction) {

			}
			//damka

		}
		return true;
	}


	void undo(int direction, int x, int y, int player, int steps = 1) {
		int enemy = ((player == 1) ? 2 : 1);
		int tmp = array[x][y];
		//pion

		switch (direction) {
		case 1: {
			//ruch o jeden
			if (inBoard(x - 1, y - 1)) {
				if (array[x - 1][y - 1] == 0) {
					//pole zajete
					//czy jest bicie
					if (inBoard(x - 2, y - 2)) {
						if (array[x - 2][y - 2] == player) {
							//sprawdz czy sa dalsze bicia/

							//zmien piony
							array[x][y] = player;
							array[x - 1][y - 1] = enemy;
							array[x - 2][y - 2] = 0;
						}
					}
				}
				else if (array[x - 1][y - 1] == player) {
					//przesuniecie pionka
					array[x][y] = player;
					array[x - 1][y - 1] = 0;
				}
			}
			break;

		}
		case 2: {
			//ruch o jeden
			if (inBoard(x - 1, y + 1)) {
				if (array[x - 1][y + 1] == 0) {
					//pole zajete
					//czy jest bicie
					if (inBoard(x - 2, y + 2)) {
						if (array[x - 2][y + 2] == player) {
							//sprawdz czy sa dalsze bicia/

							//zmien piony
							array[x][y] = player;
							array[x - 1][y + 1] = enemy;
							array[x - 2][y + 2] = 0;
						}
					}
				}
				else if (array[x - 1][y + 1] == player) {
					//przesuniecie pionka
					array[x][y] = player;
					array[x - 1][y + 1] = 0;
				}
			}
			break;

		}

		case 3: {
			//ruch o jeden
			if (inBoard(x + 1, y + 1)) {
				if (array[x + 1][y + 1] == 0) {
					//pole zajete
					//czy jest bicie
					if (inBoard(x + 2, y + 2)) {
						if (array[x + 2][y + 2] == player) {
							//sprawdz czy sa dalsze bicia/

							//zmien piony
							array[x][y] = player;
							array[x + 1][y + 1] = enemy;
							array[x + 2][y + 2] = 0;
						}
					}
				}
				else if (array[x + 1][y + 1] == player) {
					//przesuniecie pionka
					array[x][y] = player;
					array[x + 1][y + 1] = 0;
				}
			}
			break;

		}

		case 4: {
			//ruch o jeden
			if (inBoard(x + 1, y - 1)) {
				if (array[x + 1][y - 1] == 0) {
					//pole zajete
					//czy jest bicie
					if (inBoard(x + 2, y - 2)) {
						if (array[x + 2][y - 2] == 0) {
							//sprawdz czy sa dalsze bicia/

							//zmien piony
							array[x][y] = player;
							array[x + 1][y - 1] = enemy;
							array[x + 2][y - 2] = 0;
						}
					}
				}
				else if (array[x + 1][y - 1] == player) {
					//przesuniecie pionka
					array[x][y] = player;
					array[x + 1][y - 1] = 0;
				}
			}
			break;

		}

		}
	}




	int minmax(int player, int depth) {
		if (!depth) rate(player);

		int vmax = 0;
		int v;
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (array[i][j] == player) {
					int to = (player == 1) ? 3 : 5;
					for (int d = (player == 1) ? 1 : 3; d < to; d++) {
						if (move(d, i, j, player) ){
							v = rate(player);
							undo(d, i, j, player);
						}
						if (((player == 2) && (v > vmax))) {
							vmax = v; _x = i; _y = j; _dir = d;
						}
					}
				}
			}
		}

		return vmax;

	}

	void move(int depth, int CPU = 2) {

		minmax(CPU, depth);
		move(_dir, _x, _y, CPU);

	}
};
