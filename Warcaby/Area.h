#pragma once
class Area {
	int x_1;
	int y_1;

	int x_2;
	int y_2;

	int weight;
public:
	Area(int _x_1, int _y_1, int _x_2, int _y_2, int _weight) {
		x_1 = _x_1;
		x_2 = _x_2;
		y_1 = _y_1;
		y_2 = _y_2;

		weight = _weight;
	}

	bool belong(int x, int y) {
		if (x_1 <= x && x_2 >= x && y_1 <= y && y_2 >= y)
			return true;
		return false;
	}

	int getWeight() {
		return weight;
	}

};
