// Warcaby.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "Area.h"
#include "Board.h"

#include "Game.h"


int main()
{
	Board * B = new Board(10);
	B->init();
	//B->display();
	Game * G = new Game(B);
	G->run();
	getchar();
	return 0;

}

