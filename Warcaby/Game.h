#pragma once
class Game {
	Board * B;

	bool isEnd() {
		return false;
	}
	void move() {

	}
public:
	Game(Board * _B) {
		B = _B;
	}
	int run() {
		int player = 2;
		while (!isEnd()) {
			player = ((player == 1) ? 2 : 1);
			if (player == 2) {
				B->move(2, player);
			
			}
			else {
				int x, y, direction;
				B->display();
				std::cout << "Podaj x: ";
				std::cin >> x;
				std::cout << "Podaj y: ";
				std::cin >> y;
				std::cout << "Podaj kierunek: ";
				std::cin >> direction;
				B->move(direction, x, y, player);
			}
		}
		return 1;
	}
};
